import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';

var firebaseConfig = {
  apiKey: 'AIzaSyDhYQVR2-oWWE4Ayid4RiuCsme13I2gp7U',
  authDomain: 'pruebalogin-63735.firebaseapp.com',
  projectId: 'pruebalogin-63735',
  storageBucket: 'pruebalogin-63735.appspot.com',
  messagingSenderId: '806078758117',
  appId: '1:806078758117:web:993f9fa5d86f76f52fa80c',
  measurementId: 'G-QREX6T06V7',
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const auth = firebase.auth();

export default {
  firebase,
  db,
  auth,
};
