import { LOGIN, SIGN_IN } from '../actions/sessions';

const initialState = {
  name: '',
  email: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        name: action.payload.name,
        email: action.payload.email,
      };
    case SIGN_IN:
      return initialState;
    default:
      return initialState;
  }
}
