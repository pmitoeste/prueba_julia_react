import { combineReducers } from 'redux';
import sessions from './sessions';

const rootReducer = combineReducers({ sessions });

export default rootReducer;
