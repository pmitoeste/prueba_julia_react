import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 30,
  },
  textRegister: {
    fontSize: 15,
    marginVertical: 15,
    textAlign: 'center',
  },
  textInput: {
    marginVertical: 10,
  },
  errorTextInput: {
    color: 'red',
  },
});
