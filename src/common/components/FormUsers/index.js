import React, { useEffect, useState } from 'react';
import {
  Alert,
  Button,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { Input } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { encode, decode } from 'js-base64';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import style from './style';
import firebase from '../../../database/firebase';
import * as routes from '../../../navigation/routes';
import { loginUser } from '../../../actions/sessions';

const Register = ({ form, id }) => {
  const navigation = useNavigation();
  const [nameUser, setNameUser] = useState('');
  const [phoneUser, setPhoneUser] = useState('');
  const [emailUser, setEmailUser] = useState('');
  const [passUser, setPassUser] = useState('');
  const [errorAuth, setErrorAuth] = useState(false);
  const [allUsers, setAllUsers] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    firebase.db.collection('users').onSnapshot((querySnapshot) => {
      const users = [];
      querySnapshot.docs.forEach((documentSnapshot) => {
        const { name, email, phone, pass } = documentSnapshot.data();
        users.push({ name, email, phone, pass, id: documentSnapshot.id });
      });
      setAllUsers(users);
    });
  }, []);

  const userId = allUsers.find((user) => user.id === id);
  useEffect(() => {
    if (userId) {
      setNameUser(userId.name);
      setEmailUser(userId.email);
      setPhoneUser(userId.phone);
      setPassUser(userId.pass);
    }
  }, [userId]);

  const createUser = () => {
    const userFilter = allUsers.find(
      (singleUser) => singleUser.email === emailUser,
    );
    if (userFilter) {
      Alert.alert('El email ya se encuentra registrado');
      setErrorAuth(true);
    } else if (
      nameUser === '' ||
      emailUser === '' ||
      phoneUser === '' ||
      passUser === ''
    ) {
      Alert.alert('Todos los campos son obligatorios');
      setErrorAuth(true);
    } else {
      firebase.db.collection('users').add({
        name: nameUser,
        phone: phoneUser,
        email: emailUser,
        pass: encode(passUser),
      });
      firebase.auth
        .createUserWithEmailAndPassword(emailUser, passUser)
        .then(() => {
          Alert.alert('Éxito!', 'El usuario a sido creado', [
            {
              text: 'OK',
              onPress: () => {
                navigation.navigate(routes.USERLIST);
                !form && dispatch(loginUser(nameUser, emailUser));
              },
            },
            { cancelable: false },
          ]);
        });
      setErrorAuth(false);
    }
  };

  const updateUser = async () => {
    await firebase.db.collection('users').doc(id).update({
      name: nameUser,
      email: emailUser,
      phone: phoneUser,
      pass: passUser,
    });
    Alert.alert('Éxito!', 'El usuario a sido actualizado', [
      {
        text: 'OK',
        onPress: () => navigation.navigate(routes.USERLIST),
      },
      { cancelable: false },
    ]);
  };

  const deleteUser = async () => {
    await Alert.alert(
      'Estas seguro!',
      'Seguro que quiere continuar esta acción ya no se podrá deshacer',
      [
        {
          text: 'Cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            firebase.db.collection('users').doc(id).delete();
            navigation.navigate(routes.USERLIST);
          },
        },
      ],
      { cancelable: false },
    );
  };
  return (
    <View style={!form && style.viewContainer}>
      <Input
        placeholder="Julian"
        onChangeText={(e) => setNameUser(e)}
        autoCapitalize="none"
        label="Nombre del usuario"
        containerStyle={style.textInput}
        errorStyle={style.errorTextInput}
        errorMessage={errorAuth ? 'Este campo es obligatorio' : ''}
        defaultValue={form && id !== '' ? nameUser : ''}
      />
      <Input
        placeholder="+5712345678"
        label="WhatsApp completo (con el indicador de país)"
        onChangeText={(e) => setPhoneUser(e)}
        autoCapitalize="none"
        containerStyle={style.textInput}
        errorStyle={style.errorTextInput}
        errorMessage={errorAuth ? 'Este campo es obligatorio' : ''}
        defaultValue={form && id !== '' ? phoneUser : ''}
      />
      <Input
        placeholder="prueba@prueba.co"
        onChangeText={(e) => setEmailUser(e)}
        autoCapitalize="none"
        label="Email"
        textContentType="emailAddress"
        containerStyle={style.textInput}
        errorStyle={style.errorTextInput}
        errorMessage={errorAuth ? 'Este campo es obligatorio' : ''}
        defaultValue={form && id !== '' ? emailUser : ''}
      />
      <Input
        placeholder="Contraseña"
        onChangeText={(e) => setPassUser(encode(e))}
        label="Contraseña"
        autoCapitalize="none"
        secureTextEntry={form && id !== '' ? false : true}
        textContentType="password"
        errorStyle={style.errorTextInput}
        errorMessage={errorAuth ? 'Este campo es obligatorio' : ''}
        defaultValue={form && id !== '' ? decode(passUser) : ''}
      />
      <Button
        title={form && id !== '' ? 'Actualizar usuario' : 'Crear usuario'}
        onPress={form && id !== '' ? updateUser : createUser}
      />
      {form && id !== '' && (
        <Button title="Eliminar usuario" onPress={deleteUser} />
      )}
      {!form && (
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate(routes.LOGIN)}>
          <Text style={style.textRegister}>
            Si ya tienes una cuenta inicia sesión aquí
          </Text>
        </TouchableWithoutFeedback>
      )}
    </View>
  );
};

Register.propTypes = {
  form: PropTypes.bool,
  id: PropTypes.string,
};

Register.defaultProps = {
  form: false,
  id: '',
};

export default Register;
