import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as routes from './routes';
import Login from '../screens/Login';
import Register from '../screens/Register';
import UserDetail from '../screens/UserDetail';
import UserList from '../screens/UserList';
import CreateUser from '../screens/CreateUser';

const Stack = createStackNavigator();

export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={routes.LOGIN}>
        <Stack.Screen
          name={routes.LOGIN}
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={routes.REGISTER}
          component={Register}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={routes.USERLIST}
          component={UserList}
          options={{
            headerTitle: 'Listado de usuarios',
            headerLeft: false,
          }}
        />
        <Stack.Screen
          name={routes.USERDETAIL}
          component={UserDetail}
          options={{
            headerTitle: 'Detalle del usuario',
          }}
        />
        <Stack.Screen
          name={routes.CREATEUSER}
          component={CreateUser}
          options={{
            headerTitle: 'Crear un nuevo usuario',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
