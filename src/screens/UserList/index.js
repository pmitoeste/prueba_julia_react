import React, { useEffect, useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { Button, ListItem } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { signInUser } from '../../actions/sessions';
import firebase from '../../database/firebase';
import * as routes from '../../navigation/routes';
import style from './style';

const UserList = () => {
  const nameUser = useSelector((state) => state.sessions.name);
  const [totalUsers, setTotalUsers] = useState([]);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    firebase.db.collection('users').onSnapshot((querySnapshot) => {
      const users = [];
      querySnapshot.docs.forEach((documentSnapshot) => {
        const { name, email, phone, pass } = documentSnapshot.data();
        users.push({ name, email, phone, pass, id: documentSnapshot.id });
      });
      setTotalUsers(users);
    });
  }, []);

  const renderUsersLists = () => (
    <ScrollView style={style.scrollView}>
      {totalUsers.map((user) => (
        <ListItem
          key={user.id}
          bottomDivider
          onPress={() =>
            navigation.navigate(routes.USERDETAIL, {
              userId: user.id,
            })
          }>
          <ListItem.Content>
            <ListItem.Title>{user.name}</ListItem.Title>
            <ListItem.Subtitle>{user.email}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>
      ))}
    </ScrollView>
  );

  return (
    <View>
      <Text style={style.textName}>Bienvenido {nameUser}</Text>
      <Text style={style.textTitle}>
        Usuarios registrados: {totalUsers.length}
      </Text>
      <Button
        title="Crear un nuevo usuario"
        onPress={() => navigation.navigate(routes.CREATEUSER)}
      />
      {renderUsersLists()}
      <Button
        type="outline"
        title="Cerra sesión"
        onPress={() => {
          dispatch(signInUser());
          navigation.navigate(routes.LOGIN);
        }}
      />
    </View>
  );
};

export default UserList;
