import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  textName: {
    fontSize: 20,
    textAlign: 'center',
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 16,
    marginVertical: 10,
    paddingLeft: 20,
  },
  scrollView: {
    marginVertical: 20,
    maxHeight: 500,
  },
});
