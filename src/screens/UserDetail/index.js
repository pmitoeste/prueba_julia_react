import React from 'react';
import FormUsers from '../../common/components/FormUsers';

const UserDetail = (props) => {
  const { userId } = props.route.params;
  return <FormUsers form id={userId} />;
};

export default UserDetail;
