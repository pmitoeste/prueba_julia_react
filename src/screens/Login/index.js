import React, { useEffect, useState } from 'react';
import { Alert, Text, View } from 'react-native';
import { Button, Input } from 'react-native-elements';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import firebase from '../../database/firebase';
import { useDispatch, useSelector } from 'react-redux';
import { encode } from 'js-base64';

import { loginUser } from '../../actions/sessions';
import * as routes from '../../navigation/routes';
import style from './style';

const Login = () => {
  const navigation = useNavigation();
  const [user, setUser] = useState('');
  const [pass, setPass] = useState('');
  const [errorAuth, setErrorAuth] = useState(false);
  const [totalUsers, setTotalUsers] = useState([]);
  const name = useSelector((state) => state.sessions.name);
  const email = useSelector((state) => state.sessions.email);

  const dispatch = useDispatch();

  useEffect(() => {
    if (name !== '' && email !== '') {
      navigation.navigate(routes.USERLIST);
    }
  }, [name, email, navigation]);

  useEffect(() => {
    firebase.db
      .collection('users')
      .get()
      .then((querySnapshot) => {
        const users = [];
        querySnapshot.forEach((documentSnapshot) => {
          users.push(documentSnapshot.data());
        });
        setTotalUsers(users);
      });
  }, []);

  const authUser = () => {
    const filtesUser = totalUsers.find(
      (userLog) => userLog.email === user && userLog.pass === pass,
    );
    if (filtesUser) {
      dispatch(loginUser(filtesUser.name, filtesUser.email));
      navigation.navigate(routes.USERLIST);
      setErrorAuth(false);
    } else {
      Alert.alert('Email o contraseña invalidos');
      setErrorAuth(true);
    }
  };
  return (
    <View style={style.viewContainer}>
      <Input
        placeholder="Email"
        onChangeText={(e) => setUser(e)}
        autoCapitalize="none"
        textContentType="emailAddress"
        containerStyle={style.textInput}
        errorStyle={style.errorTextInput}
        errorMessage={errorAuth ? 'Este campo es obligatorio' : ''}
      />
      <Input
        placeholder="Contraseña"
        onChangeText={(e) => setPass(encode(e))}
        autoCapitalize="none"
        secureTextEntry
        textContentType="password"
        errorStyle={style.errorTextInput}
        errorMessage={errorAuth ? 'Este campo es obligatorio' : ''}
      />
      <Button title="Iniciar Sesión" onPress={authUser} />
      <TouchableWithoutFeedback
        onPress={() => navigation.navigate(routes.REGISTER)}>
        <Text style={style.textRegister}>
          Si no tienes una cuenta regístrate aquí
        </Text>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default Login;
