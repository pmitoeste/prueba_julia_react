export const LOGIN = 'LOGIN';
export const SIGN_IN = 'SIGN_IN';

export const loginUser = (name, email) => ({
  type: LOGIN,
  payload: { name, email },
});

export const signInUser = () => ({
  type: SIGN_IN,
});
